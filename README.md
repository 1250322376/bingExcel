
# bingExcel  

处理excel与java model之间转换,想做成一个excel的orm类型的框架
具体使用说明可以参照   [开源中国wiki](https://git.oschina.net/bingyulei007/bingExcel/wikis/home),wiki写的比较简单，大家可以fork下源码，里面有很多单元测试的例子。
## 环境说明
基于java1.7，依赖poi3.1X.jar包,commons-lang3 jar,com.google.guava 等
## jar 包管理地址
https://github.com/bingyulei007/mvn-repo/tree/master/repository/com/bing/excel
## 联系我
如果有问题可以留言，也可以发送邮件到 * _shizhongtao@gmail.com_ *    
也可以加入qq群进行问题交流：  
群名称：`BingExcel问题交流群`  
群   号：`414262584`  
PS:*github上的源码已经不再更新，源码全部转移到开源中国,如果要下载源码的同学请知晓* `  另外由于目前项目是个人维护，所以在功能的增加上及维护上可能有点滞后，请谅解。`

